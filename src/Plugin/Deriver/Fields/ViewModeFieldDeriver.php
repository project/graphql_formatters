<?php

namespace Drupal\graphql_formatters\Plugin\Deriver\Fields;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\Utility\StringHelper;

/**
 * Get fields from view modes with their type, entity type, bundle.
 */
class ViewModeFieldDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * Get fields from view modes with their type, entity type, bundle.
   *
   * @param array|\Drupal\Component\Plugin\Definition\PluginDefinitionInterface $base_plugin_definition
   *   The definition of the base plugin from which the derivative plugin
   *   is derived. It is maybe an entire object or just some array, depending
   *   on the discovery mechanism.
   *
   * @return array
   *   The full definition array of the derivative plugin, typically a merge of
   *   $base_plugin_definition with extra derivative-specific information. NULL
   *   if the derivative doesn't exist.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\graphql_formatters\GraphQLFormattersFields $fieldsService */
    $fieldsService = \Drupal::service('graphql_formatters.fields');
    $viewModeFields = $fieldsService->getViewModeFields();
    foreach ($viewModeFields as $key => $viewModeField) {
      if (array_key_exists('type', $viewModeField)) {
        $parent = StringHelper::camelCase('field', $viewModeField['entity_type_id'], $viewModeField['bundle'], $viewModeField['field']);
        $type = $fieldsService->getDataType($viewModeField['type']);
        if (!empty($type)) {
          $derivative = [
            'parents' => [$parent],
            'type' => $type,
            'description' => $this->t('Renders the field based on the given view mode.'),
          ] + $base_plugin_definition;

          if (!isset($derivative['arguments']['mode'])) {
            $derivative['arguments'] = isset($derivative['arguments']) ? $derivative['arguments'] : [];
            $derivative['arguments']['mode'] = [
              'type' => StringHelper::camelCase($viewModeField['entity_type_id'], 'display', 'mode', 'id'),
              'optional' => TRUE,
            ];
          }

          $this->derivatives[$key] = $derivative;
        }
      }
    }
    return $this->derivatives;
  }

}
