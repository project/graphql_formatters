<?php

namespace Drupal\graphql_formatters\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_core\Plugin\GraphQL\Fields\EntityFieldBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Provides a field formatter.
 *
 * @GraphQLField(
 *   id = "view_mode_field_formatter",
 *   name = "viewModeFieldFormatter",
 *   description = @Translation("View Mode based Field Formatter."),
 *   secure = true,
 *   deriver = "Drupal\graphql_formatters\Plugin\Deriver\Fields\ViewModeFieldDeriver"
 * )
 */
class ViewModeFieldFormatter extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (
      // @todo check instance.
      // @todo scalars.
      !$value->isEmpty() &&
      array_key_exists('value', $value->getValue())
    ) {
      $targetEntityTypeId = $value->getFieldDefinition()
        ->getTargetEntityTypeId();
      $viewModes = \Drupal::entityQuery('entity_view_mode')
        ->condition('targetEntityType', $targetEntityTypeId)
        ->execute();

      // @todo check language.
      // @todo make use of RenderContext, see EntityRendered.
      $viewMode = isset($args['mode']) ? $args['mode'] : 'full';
      if (!in_array($targetEntityTypeId . '.' . $viewMode, $viewModes)) {
        throw new \Exception('View mode does not exist.');
      }

      /** @var \Drupal\Core\Field\FieldItemListInterface $field */
      $field = $value->getParent();
      $viewBuilder = \Drupal::entityTypeManager()
        ->getViewBuilder($targetEntityTypeId);
      $view = $viewBuilder->viewField($field, $viewMode);
      $renderedView = \Drupal::service('renderer')->render($view);

      yield $renderedView;
    }
  }

}
