<?php

namespace Drupal\graphql_formatters\Plugin\GraphQL\Fields\File;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Formatted File size.
 *
 * @GraphQLField(
 *   id = "file_size_formatter",
 *   secure = true,
 *   name = "fileSizeFormatter",
 *   type = "String",
 *   arguments = {
 *     "decimals" = "Integer",
 *     "langCode" = "String",
 *   },
 *   parents = {"File"}
 * )
 */
class FileSizeFormatter extends FieldPluginBase {

  /**
   * Generates a string representation for the given byte count.
   *
   * Port of format_size() with precision as a parameter.
   *
   * @param string $size
   *   A size in bytes.
   * @param int $precision
   *   The amount of decimals.
   * @param string $langcode
   *   Optional language code to translate to a language other than what is used
   *   to display the page.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A translated string representation of the size.
   */
  private function formatSize($size, $precision, $langcode = NULL) {
    $absolute_size = abs($size);
    if ($absolute_size < Bytes::KILOBYTE) {
      return \Drupal::translation()->formatPlural($size, '1 byte', '@count bytes', [], ['langcode' => $langcode]);
    }
    // Create a multiplier to preserve the sign of $size.
    $sign = $absolute_size / $size;
    foreach (['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] as $unit) {
      $absolute_size /= Bytes::KILOBYTE;
      $rounded_size = round($absolute_size, $precision);
      if ($rounded_size < Bytes::KILOBYTE) {
        break;
      }
    }
    $args = ['@size' => $rounded_size * $sign, '@unit' => $unit];
    $options = ['langcode' => $langcode];
    return new TranslatableMarkup('@size @unit', $args, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof FileInterface) {
      $decimals = $args['decimals'] ?? 2;
      $langCode = $args['langCode'] ?? NULL;
      $languages = \Drupal::languageManager()->getLanguages();
      if (!array_key_exists($langCode, $languages)) {
        $langCode = NULL;
      }
      yield $this->formatSize($value->getSize(), $decimals, $langCode);
    }
  }

}
