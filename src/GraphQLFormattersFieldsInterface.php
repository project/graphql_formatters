<?php

namespace Drupal\graphql_formatters;

/**
 * Interface GraphQLFormattersFieldsInterface.
 */
interface GraphQLFormattersFieldsInterface {

  /**
   * Returns the fields that are used in the view modes with their type.
   *
   * @return array
   *   List of fields used in view modes.
   */
  public function getViewModeFields();

  /**
   * Returns the GraphQL data type from the field type.
   *
   * @param string $field_type
   *   Field type.
   *
   * @return string
   *   Data type.
   */
  public function getDataType($field_type);

}
