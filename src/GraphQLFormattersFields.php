<?php

namespace Drupal\graphql_formatters;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class GraphQLFormattersFields.
 */
class GraphQLFormattersFields implements GraphQLFormattersFieldsInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new GraphQLFormattersFields object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewModeFields() {
    $result = [];
    // Load all view modes for all entity types.
    $entityViewDisplays = EntityViewDisplay::loadMultiple();
    foreach ($entityViewDisplays as $entityViewDisplayKey => $entityViewDisplay) {
      // Explode the key 'entity_type.bundle.view_mode'.
      $context = explode('.', $entityViewDisplayKey);
      // Fields.
      $components = $entityViewDisplay->getComponents();
      foreach ($components as $componentKey => $component) {
        $result[$context[0] . '-' . $context[1] . '-' . $context[2] . '-' . $componentKey] = [
          'field' => $componentKey,
          'entity_type_id' => $context[0],
          'bundle' => $context[1],
          'view_mode' => $context[2],
        ];
      }
    }

    // Add the field type.
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager */
    $fieldManager = \Drupal::service('entity_field.manager');
    $fieldMap = $fieldManager->getFieldMap();
    foreach ($fieldMap as $entityTypeId => $fields) {
      foreach ($result as $viewModeFieldKey => $viewModeField) {
        if (
          is_array($viewModeField) &&
          $entityTypeId === $viewModeField['entity_type_id']
        ) {
          foreach ($fields as $field => $fieldProperties) {
            if (
              $field === $viewModeField['field'] &&
              array_key_exists($viewModeField['bundle'], $fieldProperties['bundles'])
            ) {
              if (!empty($fieldProperties['type'])) {
                $result[$viewModeFieldKey]['type'] = $fieldProperties['type'];
              }
            }
          }
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataType($field_type) {
    $result = '';
    switch ($field_type) {
      case 'string':
      case 'text':
      case 'text_with_summary':
      case 'text_long':
      case 'datetime';
        $result = 'String';
        break;

      case 'integer':
        $result = 'Int';
        break;
    }
    return $result;
  }

}
