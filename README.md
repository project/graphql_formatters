# GraphQL Formatters

GraphQL Field Formatters and renderer helpers for fields.
Applies to GraphQL 3.x only.

## Use case

When it makes sense to delegate the rendering to Drupal.

Examples:
* delegate sanitizing to Twig instead of a Javascript library
* in combination with
[GraphQL Twig](https://www.drupal.org/project/graphql_twig)
* in a progressively decoupled setup, where markup is shared between decoupled
/ non decoupled components for styling.

For helpers like:
* Text summary: truncate or reuse the field summary if available
* Plain text: strip tags of formatted texts
* Drupal date format
* Currency
* File size
* ...

## Already supported by GraphQL 3.x core

You might not need this module for these use cases,
supported by the Drupal GraphQL core:

* Format a date with inline format: `entityCreated(format: "d/m/Y")`
* Render an entity with a view mode: `entityRendered(mode: TEASER)`
* Image styles
* ...

## Dependencies

[GraphQL](https://www.drupal.org/project/graphql) 8.3.x

## View Mode based Field Formatter

WIP, does not support all data types.

Inherits from the view mode configuration of the entity bundle.

Example:

```graphql
{
  nodeQuery {
    entities {
      ... on NodeArticle {
        fieldTextFormatted {
          viewModeFieldFormatter(mode: TEASER)
        }
      }
    }
  }
}
```

## Settings based Field Formatter

WIP

Instead of inheriting from the view mode configuration, the
settings from the field are passed.

Example:

```graphql
{
  nodeQuery {
    entities {
      ... on NodeArticle {
        fieldTextFormatted {
          fieldFormatter(settings: {
            label: "hidden",
            trimmed: 600
          })
        }
      }
    }
  }
}
```

## Other field based helpers

### File size

```graphql
{
  fileQuery {
    entities {
      ... on File {
        filename
        size: fileSizeFormatter(decimals:1)
        url
      }
    }
  }
}
```

### Date based on the date formats (e.g. short, medium) - GraphQL Date

WIP

### Currency formatter

WIP
